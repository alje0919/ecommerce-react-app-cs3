const productsData = [
	
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, architecto adipisci quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, architecto adipisci quod.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, architecto adipisci quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, architecto adipisci quod.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, architecto adipisci quod. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, architecto adipisci quod.",
		price: 55000,
		onOffer: true
	}


]

export default productsData;